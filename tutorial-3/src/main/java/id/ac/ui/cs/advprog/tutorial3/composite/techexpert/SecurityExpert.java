package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    //TODO Implement
    public SecurityExpert(String name, double salary) {
        //TODO Implement
        this.name = name;
        this.salary = salary;
        checkMinSalary(70000);
        this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
