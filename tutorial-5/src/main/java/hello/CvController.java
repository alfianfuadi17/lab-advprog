package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                                 String visitor, Model model) {
        String greeting = "";
        if (visitor == null || visitor.equals("")) greeting = "This is my CV";
        else greeting = visitor + ", I hope you interested to hire me";
        model.addAttribute("greeting", greeting);
        return "cv";
    }
}
