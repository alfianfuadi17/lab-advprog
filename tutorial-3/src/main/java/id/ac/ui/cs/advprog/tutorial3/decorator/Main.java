package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Filling;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;

public class Main {
    public static void main(String[] args) {
        Food sandwich = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        sandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(sandwich);
        sandwich = FillingDecorator.CHEESE.addFillingToBread(sandwich);
        sandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(sandwich);
        sandwich = FillingDecorator.LETTUCE.addFillingToBread(sandwich);
        sandwich = FillingDecorator.TOMATO.addFillingToBread(sandwich);

        System.out.println(sandwich.getDescription());
        System.out.println("The price is $" + sandwich.cost());
    }
}
