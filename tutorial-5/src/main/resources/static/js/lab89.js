$(document).ready(function(){   
    setTimeout(show, 1000);
    function show() {
        $(".loader").css("display", "none");
        $(".bodypage").css("display", "block");
    }

    var clicked = false;
    $(".tema").click(function(){
        if (!clicked) {
            clicked = true;
            $("body").css({"background-color" : "#FEF9E7"});
        }
        else {
            clicked = false;
            $("body").css({"background-color" : "white"});
        }

    });

    $(".accordion").accordion({
        collapsible : true,
        active : false,
        heightStyle : "content",
    }); 

});