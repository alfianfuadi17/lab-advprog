package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
    //TODO Implement
    public BackendProgrammer(String name, double salary) {
        //TODO Implement
        this.name = name;
        this.salary = salary;
        checkMinSalary(20000);
        this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
