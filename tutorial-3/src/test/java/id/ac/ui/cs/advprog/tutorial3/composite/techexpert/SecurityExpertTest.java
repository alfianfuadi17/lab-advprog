package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import static org.junit.Assert.assertEquals;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;
import org.junit.Before;
import org.junit.Test;

public class SecurityExpertTest {
    private SecurityExpert securityExpert;

    @Before
    public void setUp() {
        securityExpert = new SecurityExpert("Brook", 200000.00);
    }

    @Test
    public void testMethodCost() {
        assertEquals(200000.00, securityExpert.getSalary(), 0.00);
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Brook", securityExpert.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Security Expert", securityExpert.getRole());
    }
}
