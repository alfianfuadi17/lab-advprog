package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        //TODO Implement
        this.name = name;
        this.salary = salary;
        checkMinSalary(200000);
        this.role = "CEO";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
